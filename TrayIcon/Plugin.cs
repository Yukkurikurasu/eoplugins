﻿using ElectronicObserver.Window.Plugins;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ElectronicObserver.Window;
using System.Windows.Forms;
using System.Runtime.InteropServices;

namespace TrayIcon
{
	public class Plugin : ServerPlugin
	{

		private NotifyIcon notifyIcon;
		private FormMain mainForm;

		public override string MenuTitle
		{
			get
			{
				return "托盘图标";
			}
		}

		public override bool RunService( FormMain main )
		{
			mainForm = main;
			notifyIcon = new NotifyIcon();

			notifyIcon.Icon = main.Icon;
			notifyIcon.Text = main.Text;
			notifyIcon.Visible = true;

			notifyIcon.Click += NotifyIcon_Click;

			// 最小化事件
			oldState = main.WindowState;
			main.SizeChanged += Main_SizeChanged;
			main.FormClosing += Main_FormClosing;

			return true;
		}

		private void Main_FormClosing( object sender, FormClosingEventArgs e )
		{
			if ( notifyIcon != null )
			{
				notifyIcon.Visible = false;
				notifyIcon.Dispose();
			}
		}

		private void Main_SizeChanged( object sender, EventArgs e )
		{
			if ( mainForm.WindowState == FormWindowState.Minimized )
			{
				// 隐藏
				mainForm.Visible = false;
			}
			else
			{
				oldState = mainForm.WindowState;
			}
		}

		FormWindowState oldState;

		private void NotifyIcon_Click( object sender, EventArgs e )
		{
			bool visible;
			visible = !mainForm.Visible;

			if ( visible )
			{
				mainForm.Visible = true;
				if ( mainForm.WindowState == FormWindowState.Minimized )
					mainForm.WindowState = ( oldState == FormWindowState.Minimized ? FormWindowState.Normal : oldState );
			}
			else
			{
				oldState = mainForm.WindowState;
				mainForm.WindowState = FormWindowState.Minimized;
			}

		}
	}
}
